# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('last_seen', '0005_uservisit_not_unique'),
    ]

    operations = [
        migrations.AddField(
            model_name='uservisit',
            name='referring_domain',
            field=models.CharField(max_length=32, blank=True),
        ),
        migrations.AddField(
            model_name='uservisit',
            name='utm_campaign',
            field=models.CharField(max_length=32, blank=True),
        ),
        migrations.AddField(
            model_name='uservisit',
            name='utm_content',
            field=models.CharField(max_length=32, blank=True),
        ),
        migrations.AddField(
            model_name='uservisit',
            name='utm_medium',
            field=models.CharField(max_length=32, blank=True),
        ),
        migrations.AddField(
            model_name='uservisit',
            name='utm_source',
            field=models.CharField(max_length=32, blank=True),
        ),
        migrations.AddField(
            model_name='uservisit',
            name='utm_term',
            field=models.CharField(max_length=32, blank=True),
        ),
    ]
