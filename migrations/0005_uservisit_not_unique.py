# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('last_seen', '0004_uservisit'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='uservisit',
            unique_together=set([]),
        ),
    ]
