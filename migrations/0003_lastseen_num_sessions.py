# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('last_seen', '0002_lastseen_extra_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='lastseen',
            name='num_sessions',
            field=models.IntegerField(default=1),
        ),
    ]
