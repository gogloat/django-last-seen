# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('last_seen', '0006_marketing_fields'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uservisit',
            name='referring_domain',
            field=models.CharField(max_length=255, blank=True),
        ),
    ]
