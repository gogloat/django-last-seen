# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.utils.timezone
import jsonfield.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('last_seen', '0003_lastseen_num_sessions'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserVisit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('module', models.CharField(default=b'default', max_length=20)),
                ('ip_address', models.GenericIPAddressField(null=True)),
                ('session_key', models.CharField(max_length=40)),
                ('extra_data', jsonfield.fields.JSONField(default=dict)),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('site', models.ForeignKey(to='sites.Site', on_delete=models.CASCADE)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ('-date',),
            },
        ),
        migrations.AlterUniqueTogether(
            name='uservisit',
            unique_together=set([('user', 'site', 'module')]),
        ),
    ]
