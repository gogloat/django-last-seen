# -*- coding: utf-8 -*-


from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('last_seen', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='lastseen',
            name='extra_data',
            field=jsonfield.fields.JSONField(default=dict),
        ),
    ]
