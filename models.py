import time
import datetime
import logging
import urllib.parse

from django.db import models
from django.contrib.sites.models import Site
from django.utils import timezone
from django.core.cache import cache
from . import settings
from django.conf import settings as django_settings
import jsonfield

logger = logging.getLogger(__name__)


class LastSeenManager(models.Manager):
    """
    Manager for LastSeen objects

    Provides 2 utility methods
    """

    @staticmethod
    def get_client_ip(request):
        x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
        if x_forwarded_for:
            ip = x_forwarded_for.split(",")[0]
        else:
            ip = request.META.get("REMOTE_ADDR")
        return ip

    def seen(
        self,
        user,
        request,
        module=settings.LAST_SEEN_DEFAULT_MODULE,
        site=None,
        force=False,
    ):
        """
        Mask an user last on database seen with optional module and site

        If module not provided uses LAST_SEEN_DEFAULT_MODULE from settings
        If site not provided uses current site

        The last seen object is only updates is LAST_SEEN_INTERVAL seconds
        passed from last update or force=True
        """
        ip_address = self.get_client_ip(request)
        session_key = request.session.session_key or ""
        extra_data = {
            "user-agent": request.META.get("HTTP_USER_AGENT"),
        }
        if request.COOKIES.get("fs_uid"):
            extra_data["fs_uid"] = request.COOKIES["fs_uid"]

        for key_in_session in settings.LAST_SEEN_SAVE_SESSION_VALUES:
            if key_in_session not in request.session:
                continue
            extra_data[key_in_session] = request.session[key_in_session]

        if not site:
            site = Site.objects.get_current()
        seen, created = self.get_or_create(
            user=user,
            site=site,
            module=module,
            defaults={
                "ip_address": ip_address,
                "session_key": session_key,
                "extra_data": extra_data,
            },
        )
        if created:
            logger.info(
                "%s %s %s first"
                % (
                    user.username,
                    ip_address,
                    session_key,
                )
            )
            return seen

        # if we get the object, see if we need to update
        limit = timezone.now() - datetime.timedelta(seconds=settings.LAST_SEEN_INTERVAL)
        if seen.session_key != session_key or seen.last_seen < limit or force:
            # enough time has passed for us to rewrite 'last seen' -- but is it a separate session?
            if (
                timezone.now() - seen.last_seen
            ).total_seconds() > settings.LAST_SEEN_INTERVAL * 2:
                seen.num_sessions += 1

            seen.last_seen = timezone.now()
            seen.ip_address = ip_address
            seen.session_key = session_key or ""
            if not seen.extra_data:
                seen.extra_data = {}
            seen.extra_data.update(extra_data)
            seen.save(
                update_fields=(
                    "last_seen",
                    "ip_address",
                    "session_key",
                    "extra_data",
                    "num_sessions",
                )
            )
            logger.info(
                "%s %s %s not first"
                % (
                    user.username,
                    ip_address,
                    session_key,
                )
            )

        return seen

    def when(self, user, module=None, site=None):
        args = {"user": user}
        if module:
            args["module"] = module
        if site:
            args["site"] = site
        return self.filter(**args).latest("last_seen").last_seen

    def seen_recently(self, user, module=settings.LAST_SEEN_DEFAULT_MODULE, site=None):
        if site is None:
            site = Site.objects.get_current()
        return LastSeen.objects.filter(
            module=module,
            site=site,
            user=user,
            last_seen__gte=timezone.now() - datetime.timedelta(hours=1),
        ).exists()


class AbstractUserVisit(models.Model):
    site = models.ForeignKey(Site, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    module = models.CharField(default=settings.LAST_SEEN_DEFAULT_MODULE, max_length=20)
    ip_address = models.GenericIPAddressField(null=True)
    session_key = models.CharField(
        max_length=40, null=False
    )  # not foreign key - don't care about integrity (session can get deleted, but logs abide)
    extra_data = jsonfield.JSONField()

    objects = LastSeenManager()

    class Meta:
        abstract = True


class UserVisit(AbstractUserVisit):
    date = models.DateTimeField(default=timezone.now)
    referring_domain = models.CharField(max_length=255, blank=True)
    utm_source = models.CharField(max_length=32, blank=True)
    utm_campaign = models.CharField(max_length=32, blank=True)
    utm_medium = models.CharField(max_length=32, blank=True)
    utm_term = models.CharField(max_length=32, blank=True)
    utm_content = models.CharField(max_length=32, blank=True)

    MARKETING_FIELDS = (
        "referring_domain",
        "utm_source",
        "utm_campaign",
        "utm_medium",
        "utm_term",
        "utm_content",
    )

    def __str__(self):
        return "%s on %s" % (self.user, self.date)

    class Meta:
        ordering = ("-date",)


class LastSeen(AbstractUserVisit):
    last_seen = models.DateTimeField(default=timezone.now)
    num_sessions = models.IntegerField(default=1)

    def __str__(self):
        return "%s on %s" % (self.user, self.last_seen)

    class Meta:
        unique_together = (("user", "site", "module"),)
        ordering = ("-last_seen",)

    def save(self, *args, **kwargs):
        super(LastSeen, self).save(*args, **kwargs)

        values = {
            mf: self.extra_data[mf]
            for mf in UserVisit.MARKETING_FIELDS
            if self.extra_data.get(mf)
        }

        values.update(
            {
                "site_id": self.site_id,
                "user_id": self.user_id,
                "module": self.module,
                "ip_address": self.ip_address,
                "session_key": self.session_key,
                "extra_data": self.extra_data,
            }
        )
        UserVisit.objects.create(**values)


def get_cache_key(site, module, user, session_key):
    """
    Get cache database to cache last database write timestamp
    """
    return "last_seen:%s:%s:%s:%s" % (site.id, module, user.pk, session_key)


def get_cached_seen(site, module, user, session_key):
    cache_key = get_cache_key(site, module, user, session_key)
    return cache.get(cache_key)


def user_seen(user, request, module=settings.LAST_SEEN_DEFAULT_MODULE, site=None):
    """
    Mask an user last seen on database if LAST_SEEN_INTERVAL seconds
    have passed from last database write.

    Uses optional module and site

    If module not provided uses LAST_SEEN_DEFAULT_MODULE from settings
    If site not provided uses current site
    """
    if not site:
        site = Site.objects.get_current()
    if module == settings.LAST_SEEN_DEFAULT_MODULE:
        module = get_module_from_request(request) or module
        if module == "path" and not user.userprofile.dont_show_again.step3.is_set:
            # user will be redirected to onboarding if tried to go to path but hasn't finish onboarding yet
            module = "onboarding"

    cache_key = get_cache_key(site, module, user, request.session.session_key or "")
    # compute limit to update db
    limit = time.time() - settings.LAST_SEEN_INTERVAL
    seen = cache.get(cache_key)
    if not seen or seen < limit:
        # mark the database and the cache, if interval is cleared force
        # database write
        if seen == -1:
            LastSeen.objects.seen(user, request, module=module, site=site, force=True)
        else:
            LastSeen.objects.seen(user, request, module=module, site=site)
        timeout = settings.LAST_SEEN_INTERVAL
        cache.set(cache_key, time.time(), timeout)
        from .signals import user_seen as user_seen_signal

        user_seen_signal.send(user, user=user, request=request)


def get_module_from_path(path):
    for path_pat, module in getattr(django_settings, "LAST_SEEN_MODULE_PATHS", []):
        if path_pat.match(path):
            return module


def get_module_from_request(request):
    module = get_module_from_path(request.path)
    if not module:
        referer = request.META.get("HTTP_REFERER")
        if referer:
            parts = urllib.parse.urlparse(referer)
            module = get_module_from_path(parts.path)

    if module and request.session.get("LAST_SEEN_MODULE") != module:
        request.session["LAST_SEEN_MODULE"] = module

    return request.session.get(
        "LAST_SEEN_MODULE"
    )  # None if couldn't figure it out now nor before


def clear_interval(user):
    """
    Clear cached interval from last database write timestamp

    Usefuf if you want to force a database write for an user
    """
    keys = {}
    for last_seen in LastSeen.objects.filter(user=user):
        cache_key = get_cache_key(
            last_seen.site, last_seen.module, user, last_seen.session_key
        )
        keys[cache_key] = -1

    if keys:
        cache.set_many(keys)
