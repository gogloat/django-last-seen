from django.contrib import admin
from django.urls import reverse

from .models import LastSeen, UserVisit
from django.utils.html import escape


class AbstractUserVisitAdmin(admin.ModelAdmin):
    search_fields = ("user__username",)
    readonly_fields = ("user",)

    def userr(self, instance):
        return '<a href="%s">%s</a> (%s)' % (
            reverse("recruit_admin:user_summary", kwargs={"user_id": instance.user.pk}),
            escape(instance.user.username),
            instance.user.pk,
        )

    userr.allow_tags = True


class LastSeenAdmin(AbstractUserVisitAdmin):
    list_filter = ("site", "module", "last_seen")
    list_display = (
        "module",
        "ip_address",
        "userr",
        "last_seen",
        "session_key",
    )
    ordering = ("-last_seen",)


class UserVisitAdmin(AbstractUserVisitAdmin):
    list_filter = ("site", "module", "date")
    list_display = (
        "ip_address",
        "userr",
        "date",
        "session_key",
        "client_family",
        "module",
    )
    ordering = ("-date",)

    def client_family(self, instance):
        return (instance.extra_data or {}).get("client_family")


admin.site.register(LastSeen, LastSeenAdmin)
admin.site.register(UserVisit, UserVisitAdmin)
