import django.dispatch

user_seen = django.dispatch.Signal(providing_args=["user", "ip_address"])
